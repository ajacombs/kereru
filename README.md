# Kererū

## *Email template build tool*

A build system for compiling and deploying email templates to Mailchimp.

If emails are like messages from a carrier pigeon, then email newsletters are
surely majestic Kererū.

Developed to aide in maintaining a large family of templates which shared a lot
of common formatting and components.

Features include:

- Jinja template compilation.
- SCSS compilation using libsass.
- Unused CSS removal & media query combination.
- Code beautifying using HTMLTidy.
- Uploading of built templates directly to Mailchimp.
- Upload of local image files to Mailchimp CDN during template build, allowing
  URL to be included in template HTML. Includes local cache (which can be
  optionally ignored) to speed up build times, and the ability to only upload
  images if they have been changed.
- Optional inlining of CSS usng Mailchimp CSS inliner API.
- Test email sending via SMTP.
- Live reloading local development server.
