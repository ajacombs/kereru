from setuptools import setup, find_packages

setup(
    name='kereru',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'click>=7.0',
        'cssselect>=1.0',
        'cssutils>=1.0',
        'Jinja2>=2.9',
        'libsass>=0.12',
        'livereload>=2.5',
        'lxml>=4.3',
        'mailchimp3>=3.0',
        'requests>=2.22',
        'ruamel.yaml>=0.16'
    ],
    entry_points='''
        [console_scripts]
        kereru=kereru.cli:cli
    ''',
)