import platform
import subprocess
import sys
from typing import Optional, Dict
from pathlib import Path

OPTIONS = {
    'newline': 'LF',
    'output-html': 'yes',
    'merge_divs': 'no',
    'merge_spans': 'no',
    'wrap': '0',
    'wrap-attributes': 'no',
    'indent-attributes': 'no',
    'indent': 'yes',
    'tidy-mark': 'no',
    # Needed to stop foo--bar being escape to foo==bar. This escaping is needed
    # for backwards compatable support for SGML, which we don't need, and would
    # break class names in that format.
    'fix-bad-comments': 'no',
    'preserve-entities': 'yes'
}

HTML_TIDY_DIR: Path = Path(__file__).parent / 'htmltidy'


def get_tidy_bin() -> Optional[str]:
    # Find the htmltidy library file for this architecture
    if sys.platform.startswith('win32'):  # Windows
        architecture = platform.architecture()[0]
        if architecture == '32bit':  # 32 bit Windows
            tidy_bin = HTML_TIDY_DIR / 'tidy-win32.exe'
        elif architecture == '64bit':  # 64 bit Windows
            tidy_bin = HTML_TIDY_DIR / 'tidy-win64.exe'
        else:
            tidy_bin = None
    # TODO: add binaries for Mac OS and Linux
    elif sys.platform.startswith('darwin'):  # Mac OS
        tidy_bin = HTML_TIDY_DIR / 'tidy-darwin'
    elif sys.platform.startswith('linux'):  # Linux
        tidy_bin = HTML_TIDY_DIR / 'tidy-linux'
    else:
        tidy_bin = None
    # Return a string of the path to the binary if it exists
    if tidy_bin is not None:
        tidy_bin = tidy_bin.resolve()
        if tidy_bin.exists():
            return str(tidy_bin)
    # Otherwise return None
    return None


def tidy_html(raw_html: str, options: Optional[Dict[str, str]] = None):
    if options is None:
        options = OPTIONS
    tidy_bin = get_tidy_bin()
    if tidy_bin is None:
        return None
    else:
        args = [tidy_bin]
        for option, value in options.items():
            args.append(f'--{option}')
            args.append(str(value))
        raw_html = raw_html.encode('utf8')
        result = subprocess.run(args, input=raw_html, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        tidied_html = result.stdout.decode('utf8')
        return tidied_html
