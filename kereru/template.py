import datetime
import re
import shutil
import subprocess
import unicodedata
from pathlib import Path
from typing import TYPE_CHECKING, Dict, Callable, Set

import cssutils
import jinja2
import lxml.html
import sass
from dataclasses import dataclass, field

from .image import resolve_image_filename
from .html_tidy import tidy_html
from .mailchimp_inliner import inline_css as mailchimp_inline_css

# These are only used for the type hints in, but would cause a circular
# dependency if imported normally. When used as a type hint they need to be
# quoted (foo: 'Variable' rather than foo: Variable) to avoid an error at runtime.
if TYPE_CHECKING:
    from .project import Variable, Project


CSS_REMOVAL_BLACKLIST = {
    '.cke_editable'
}


@dataclass
class Template:
    project: 'Project'
    shortname: str
    name: str
    template_file: str
    version: str
    variables: Dict[str, 'Variable'] = field(default_factory=dict)

    def __repr__(self):
        return f'Template(name="{self.name}")'

    def compile(
        self,
        build_details: str,
        image_resolver_func: Callable[[str], str] = resolve_image_filename,
        disable_media_queries: bool = False,
        inline_css: bool = False,
        dev_mode: bool = False
    ) -> str:
        """
        Compile the template to HTML.
        """
        # create jinja environment
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader([self.project.templates_dir, self.project.components_dir]),
            trim_blocks=True)
        # add custom filters to jinja environment
        env.filters['format_attributes'] = format_attributes
        env.filters['resolveimage'] = image_resolver_func
        env.filters['compile_scss'] = compile_scss
        env.filters['sanitise_id'] = sanitise_id
        # load variables from config into environment globals
        env.globals.update({k: v.value for k, v in self.variables.items()})
        env.globals.update(__DEVMODE=dev_mode)
        # add attributes needed by compile_scss to environment
        env.extend(variables=self.variables)
        env.extend(styles_dir=self.project.styles_dir)
        env.extend(disable_media_queries=disable_media_queries)
        # render the template
        template = env.get_template(self.template_file)
        try:
            raw_html = template.render(build_details=build_details)
        except jinja2.TemplateSyntaxError as e:
            raise RuntimeError(
                f'Jinja syntax error in \'{e.name}\' at line {e.lineno}: {e.message}')
        # remove any unused css from the file
        raw_html = remove_unused_css(
            raw_html,
            blacklist=CSS_REMOVAL_BLACKLIST,
            remove_media_queries=disable_media_queries)
        # optionally inline the CSS using the mailchimp inliner service
        if inline_css is True:
            raw_html = mailchimp_inline_css(raw_html)
        # format the compiled html using htmltidy
        pretty_html = tidy_html(raw_html)
        # return the tidied html
        return pretty_html

    @property
    def build_message(self):
        """
        Version, timestamp plus git commit hash.
        """
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %I:%M%p')
        message = f'version {self.version} built on {timestamp}'
        if shutil.which('git') is not None:
            git_stdout = subprocess.check_output(
                ['git', 'rev-parse', '--short', 'HEAD'])
            git_hash = git_stdout.decode().strip()
            message += f' from commit {git_hash}'
        return message


@jinja2.environmentfilter
def compile_scss(env, scss_file):
    # resolve the file path and check it both exists and is a file
    scss_file_path = Path(Path.cwd() / scss_file).resolve()
    if not scss_file_path.exists():
        raise RuntimeError(f'"{scss_file_path}" does not exist')
    if not scss_file_path.is_file():
        raise RuntimeError(f'"{scss_file_path}" is not a file')

    def compile_vars(variables: Dict[str, 'Variable']) -> str:
        """Compile the variables from the config file to scss `$foo: bar;` format"""
        compiled_vars = []
        for name, var in variables.items():
            if var.type_ in ('dict', 'list', 'htmlonly'):
                continue
            if var.type_ == 'pixels':
                compiled_vars.append(f'${name}: {var.value}px;')
            else:
                compiled_vars.append(f'${name}: {var.value};')
        return '\n'.join(compiled_vars)

    # join the compiled scss format variables and the content of the file together
    raw_scss = compile_vars(env.variables) + scss_file_path.read_text()
    # compile the styles
    try:
        compiled_css = sass.compile(
            string=raw_scss, include_paths=[str(env.styles_dir)])
    except sass.CompileError as e:
        raise RuntimeError(f'Error compiling {scss_file_path}\n{e}')
    # move the media queries together at the bottom of the file
    return compiled_css


def format_attributes(attributes=None, defaults=None) -> str:
    """
    Custom jinja2 filter to format a dictionary into html attributes.
    Takes a dictionary, and returns a string of format key="val".
    Keys with a value of True will be added without a value.
    """
    if attributes is None:
        attributes = {}
    if defaults is None:
        defaults = {}
    defaults.update(attributes)
    attributes = defaults
    formatted_attributes = []
    disallowed_values = [None, '', True, False]
    for k, v in attributes.items():
        if v not in disallowed_values:
            formatted_attributes.append(f'{k}="{v}"')
        elif v is True:
            formatted_attributes.insert(0, k)
    return ' '.join(formatted_attributes)


def remove_unused_css(
    raw_html: str,
    blacklist: Set[str],
    skip_elements: bool = True,
    skip_mailchimp_style_tags: bool = True,
    remove_media_queries: bool = False
) -> str:
    # Parse the HTML
    doc: lxml.html.HtmlElement = lxml.html.fromstring(raw_html, parser=lxml.html.HTMLParser(encoding='utf8'))
    # Find the css using regex rather than the parsed lxml document, as we need to later
    # sub in the changed css using regex, so we use the same approach for both parts.
    # We sub in the changed css using regex to avoid lxml's parsing, converting, and
    # re-encoding of escaped HTML entities, which we want to keep the same as the
    # raw html
    style_match = re.search(r'<style.+?>(.+?)</style>', raw_html, re.DOTALL)
    if style_match is not None:
        raw_css = style_match.group(1)
    else:
        return raw_html
    # Parse the CSS
    css_sheet: cssutils.css.CSSStyleSheet = cssutils.parseString(raw_css, validate=False)

    # Iterate through the parsed CSS to collect every CSS selector
    all_selectors = set()
    rules = list(css_sheet)
    for n, rule in enumerate(rules):
        # Optionally add to the blacklist any selector which follows the MailChimp
        # style drop-down magic comment syntax. These may not be present in the
        # HTML, but we still want the selector in the final CSS to allow users to
        # access the style in the MailChimp interface
        if rule.typeString == 'COMMENT':
            if skip_mailchimp_style_tags and rule.cssText.startswith('/**\n* @tab'):
                next_rule = rules[n + 1]
                blacklist.update(
                    selector.selectorText
                    for selector in next_rule.selectorList)
            continue
        # If the rule is a simple style rule, we want to iterate through its selectors
        if rule.typeString == 'STYLE_RULE':
            rule_selectors = list(rule.selectorList)
        # If the rule is a media query, we want to collect the
        # selectors used for all rules inside of it
        elif rule.typeString == 'MEDIA_RULE':
            rule_selectors = [
                s for inner_rule in rule.cssRules
                for s in inner_rule.selectorList]
        else:
            continue
        for selector in rule_selectors:
            selector_text = selector.selectorText
            # Ignore the selector if it is in the blacklist
            if selector_text in blacklist:
                continue
            # if skip_elements is True, ignore the selector if it targets an element
            if skip_elements and selector.element is not None:
                continue
            # if it is a pseudo selector, add the non-pseudo part of the selector
            if ':' in selector_text:
                selector_text = selector_text.split(':')[0]
            # If we haven't ignored it, add the selector to the set of all selectors
            all_selectors.add(selector_text)

    # Which selectors in the CSS do not match any elements in the HTML
    selectors_to_remove = set(
        selector for selector in all_selectors
        if not doc.cssselect(selector))

    # Convert css_sheet to a list before iterating so we aren't
    # mutating the object as we are iterating through it
    deleted_count = 0
    media_queries = {}
    for i, rule in enumerate(list(css_sheet)):
        if rule.typeString == 'STYLE_RULE':
            # if all the selectors are in selectors_to_remove,
            # then delete this rule from css_sheet
            selectors = set(s.selectorText for s in rule.selectorList)
            if selectors.issubset(selectors_to_remove):
                css_sheet.deleteRule(i - deleted_count)
                deleted_count += 1
        # Handle media queries
        elif rule.typeString == 'MEDIA_RULE':
            inner_deleted_count = 0
            for j, inner_rule in enumerate(list(rule.cssRules)):
                selectors = set(s.selectorText for s in inner_rule.selectorList)
                # if all the selectors are in selectors_to_remove,
                # then delete this rule from the media query
                if selectors.issubset(selectors_to_remove):
                    rule.deleteRule(j - inner_deleted_count)
                    inner_deleted_count += 1
            # the text of the media query itself
            media_query_text = rule.media.mediaText
            # if this query text is already in the media_queries dict, add all rules
            # inside it to the media query object there
            if media_query_text in media_queries:
                for inner_rule in rule.cssRules:
                    media_queries[media_query_text].add(inner_rule)
            # otherwise add this media query object to the dict
            else:
                media_queries[media_query_text] = rule
            # remove the media query from css_sheet for now. we'll readd it at the end
            # of the sheet momentarily
            css_sheet.deleteRule(i - deleted_count)
            deleted_count += 1

    # re-add all the media queries at the end of css_sheet
    if remove_media_queries is False:
        for rule in media_queries.values():
            css_sheet.add(rule)

    # Set global cssutils formatting options
    cssutils.ser.prefs.indent = '  '
    cssutils.ser.prefs.indentClosingBrace = False
    cssutils.ser.prefs.omitLastSemicolon = False

    # Return the edited CSS stylesheet as a string
    trimmed_css = css_sheet.cssText.decode('utf8')
    # style_el.text = trimmed_css
    # new_html = lxml.html.tostring(doc, encoding='unicode')
    new_html = re.sub(
        r'(<style.+?>).+?(</style>)',
        f'\\1\n{trimmed_css}\\2',
        raw_html,
        count=1,
        flags=re.DOTALL
    )
    return new_html


def sanitise_id(input_str: str, separator: str = '-') -> str:
    """
    Sanitises a string for use as an HTML id.

    Converts the input string to one containing only A-Z, a-z, 0-9, -, and _.
    Any accented characters will be replaced with their unaccented form
    (presuming this is an ASCII letter, else it will be skipped).
    Whitespace is replaced with the supplied separator, by default -.
    """
    sanitised_str = ''
    for char in input_str:
        # get the unicode name of the character
        unicodename = unicodedata.name(char)
        # check if the character is an accented character
        cutoff = unicodename.find(' WITH ')
        if cutoff != -1:
            # if the character has an accent, replace it with the
            # unaccented version (the same unicode name up to the WITH)
            unicodename = unicodename[:cutoff]
            char = unicodedata.lookup(unicodename)
        # add any valid characters to the output string
        if re.match(r'[A-Za-z0-9-_]', char):
            sanitised_str += char
        # if the character is a space, add a hyphen to the output string
        if re.match(r'\s', char):
            sanitised_str += separator
    return sanitised_str
