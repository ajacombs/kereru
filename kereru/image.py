import hashlib
import shutil
from base64 import b64encode
from collections import defaultdict
from pathlib import Path
from typing import Dict, Optional, TYPE_CHECKING

import requests
from ruamel.yaml import YAML
from dataclasses import dataclass

from .logging import log_errors, logger

if TYPE_CHECKING:
    from .account import Account

yaml = YAML(typ='safe')
yaml.default_flow_style = False


class ImageCache:
    """
    Caches details of uploaded images in a YAML file.
    """
    def __init__(self, cache_file: Path):
        self.cache_file: Path = cache_file
        self._store: Dict[str, 'RemoteImage'] = {}
        self._load_from_disk()

    def _load_from_disk(self) -> None:
        with self.cache_file.open(encoding='utf8') as f:
            cached_images = yaml.load(f)
        if cached_images is not None:
            for account_name, images in cached_images.items():
                for image_details in images:
                    image = RemoteImage(account_name=account_name, **image_details)
                    self._store[image.key] = image

    def _save_to_disk(self) -> None:
        doc = defaultdict(list)
        for image in self._store.values():
            doc[image.account_name].append(image.for_cache)
        doc = dict(doc)
        with self.cache_file.open('w', encoding='utf8') as f:
            yaml.dump(doc, f)

    def update(self, image: 'RemoteImage') -> None:
        if not isinstance(image, RemoteImage):
            raise ValueError(
                'Only Image objects can be added to the image cache')
        self._store[image.key] = image
        self._save_to_disk()

    def get(self, file_name: str, account_name: str) -> Optional['RemoteImage']:
        return self._store.get(f'{file_name}_{account_name}', None)


@dataclass
class LocalImage:
    path: Path

    def __post_init__(self):
        self._bytes: bytes = None

    @property
    def name(self) -> str:
        return self.path.name

    def get_bytes(self, cache: bool = True) -> bytes:
        if self._bytes is None or cache is not True:
            self._bytes = self.path.read_bytes()
        return self._bytes

    def get_hash(self, cache: bool = True) -> str:
        return hashlib.sha256(self.get_bytes(cache)).hexdigest()

    def as_base_64(self, cache: bool = True) -> str:
        return b64encode(self.get_bytes(cache)).decode('utf-8')

    def __repr__(self):
        return f'LocalImage(name="{self.name}")'


@dataclass
class RemoteImage:
    account_name: str
    name: str
    id: int
    folder_id: int
    url: str

    def __post_init__(self):
        self._bytes: bytes = None

    @property
    def key(self) -> str:
        return f'{self.name}_{self.account_name}'

    def get_hash(self, cache: bool = True) -> str:
        return hashlib.sha256(self.get_bytes(cache)).hexdigest()

    @log_errors
    def get_bytes(self, cache: bool = True) -> bytes:
        if self._bytes is None or cache is not True:
            r = requests.get(self.url)
            self._bytes = r.content
        return self._bytes

    @property
    def for_cache(self) -> Dict[str, str]:
        return {
            'name': self.name,
            'id': self.id,
            'folder_id': self.folder_id,
            'url': self.url}

    def __repr__(self):
        return f'RemoteImage(name="{self.name}")'


def resolve_image_filename(image_name: str) -> str:
    """
    Resolves the supplied image filename to itself.

    Aka returns the string that is passed to it...
    """
    return image_name


def resolve_image_mailchimp(
    image_name: str,
    account: 'Account',
    use_cached: bool = True,
    replace_all: bool = False,
    replace_changed: bool = False
) -> str:
    """
    Uploads images to a MailChimp account and returns the URL.

    When used for template compilation - which expects a function which takes a single
    string argument of the image name and returns a string of the resolved URL - it must
    be passed as a functools.partial instance which has had an Account supplied, as
    well as any optional flags set.

    Args:
        image_name: The filename of an image located in the project images folder.
        account: An Account instance of the MailChimp to upload the image to.
        use_cached: If True, if an image is contained in the project ImageCache,
            then the URL contained in the cache will be returned without
            reuploading the image.
        replace_all: If True, images will be uploaded regardless of if they are in
            the cache. If the image is already in the MailChimp account, it will be
            deleted and replaced.
        replace_changed: If True, images will be uploaded regardless of if they are
            in the cache, but only if the local version is different from the remote
            version. Each remote image will be downloaded and have it's hash compared
            to the local image file to detect if it has changed.

    Returns: A URL of the image as uploaded to the MailChimp account.
    """
    cache = account.project.image_cache
    # If the image is in the cache we can return early
    if replace_all is False and replace_changed is False and use_cached is True:
        cached_remote_image = cache.get(image_name, account.name)
        if cached_remote_image is not None:
            return cached_remote_image.url

    # If the image wasn't in the cache, start by resolving the local image file
    local_image = LocalImage(account.project.images_dir / image_name)
    if not local_image.path.exists():
        logger.error(
            f'Image "{image_name}" does not exist in the '
            f'project image folder {account.project.images_dir}. '
            f'Returning the filename as a fallback.')
        return image_name

    # Check if this image is already in the account
    existing_remote_image = account.get_existing_image(local_image)
    # If the image isn't already in the account, upload it and return the URL
    if existing_remote_image is None:
        logger.info(f'Uploading image "{image_name}" to account "{account.name}"')
        remote_image = account.upload_image(local_image)
        cache.update(remote_image)
        return remote_image.url

    # If we want to replace the image regardless of what the remote image is,
    # then delete the existing image, reupload, and return the new URL
    if replace_all is True:
        logger.info(f'Uploading image "{image_name}" to account "{account.name}"')
        account.delete_image(existing_remote_image.id)
        updated_remote_image = account.upload_image(local_image)
        cache.update(updated_remote_image)
        return updated_remote_image.url

    # If we want to replace the image only if it has changed from the local image...
    elif replace_changed is True:
        # Get hashes of both images
        local_hash = local_image.get_hash()
        remote_hash = existing_remote_image.get_hash()
        # If they match, then return the existing URL
        if local_hash == remote_hash:
            logger.info(f'Skipping uploading image "{image_name}" as it is unchanged')
            return existing_remote_image.url
        # If they don't match, delete the image and return
        else:
            logger.info(f'Uploading image "{image_name}" to account "{account.name}"')
            account.delete_image(existing_remote_image.id)
            updated_remote_image = account.upload_image(local_image)
            return updated_remote_image.url

    # If neither of those options are set, we can just return the existing URL
    else:
        cache.update(existing_remote_image)
        return existing_remote_image.url


def resolve_image_dev(image_name: str, src_dir: Path, dst_dir: Path) -> str:
    # logger = get_logger('kereru')
    image_path = src_dir / image_name
    if not image_path.exists():
        logger.error(f'"{image_path}" does not exist')
        return ''
    if not image_path.is_file():
        logger.error(f'"{image_path}" is not a file')
        return ''
    shutil.copy(str(image_path), str(dst_dir))
    return image_name
