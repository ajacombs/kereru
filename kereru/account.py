import functools
from copy import copy
from typing import Dict, TYPE_CHECKING, Optional
from mailchimp3 import MailChimp

from .image import LocalImage, RemoteImage, resolve_image_mailchimp
from .logging import logger, log_errors
from .template import Template

if TYPE_CHECKING:
    from .template import Template
    from .project import Project

Deferred = Optional


class Account:
    def __init__(
        self,
        project: 'Project',
        name: str,
        api_key: str,
    ):
        self.project: 'Project' = project
        self.name: str = name
        try:
            self.api: MailChimp = MailChimp(api_key)
        except ValueError as e:
            logger.error(
                f'Invalid API key for MailChimp account "{name}"')
            raise e
        self._remote_image_folder_id: Deferred[int] = None
        self._remote_images: Deferred[Dict[str, RemoteImage]] = None

    @log_errors
    def deploy_template(
        self,
        template: 'Template',
        use_cached_images: bool = True,
        replace_all_images: bool = False,
        replace_changed_images: bool = False
    ):
        """
        Uploads a compiled template to MailChimp. If a template with the same
        name already exists in the specified MailChimp account, it will update
        the existing template.
        """
        # Copy to avoid mutating supplied template
        template = copy(template)
        # Get all templates which are currently in this MailChimp account
        existing_templates = self.api.templates.all(
            get_all=True, type='user')['templates']
        # Iterate through any existing templates and see if there
        # is one which matches the supplied name
        existing_template_id = None
        for existing_template in existing_templates:
            if existing_template['name'] == template.name:
                existing_template_id = existing_template['id']
                logger.info(
                    f'A template named "{template.name}" already exists in '
                    f'MailChimp account "{self.name}"')
        image_resolver_func = functools.partial(
            resolve_image_mailchimp,
            account=self,
            use_cached=use_cached_images,
            replace_all=replace_all_images,
            replace_changed=replace_changed_images)
        # Data payload for MailChimp API request
        data = {
            'name': template.name,
            'html': template.compile(template.build_message, image_resolver_func)}
        # Replace the template if it already exists
        if existing_template_id:
            self.api.templates.update(existing_template_id, data)
            logger.info(
                f'Successfully updated template "{template.name}" in '
                f'MailChimp account "{self.name}"')
        # Or create a new one if it doesn't
        else:
            self.api.templates.create(data)
            logger.info(
                f'Successfully uploaded template to '
                f'MailChimp account "{self.name}"')

    @property
    def remote_image_folder_id(self):
        if self._remote_image_folder_id is None:
            self._remote_image_folder_id = self._get_remote_image_folder_id()
        return self._remote_image_folder_id

    @log_errors
    def _get_remote_image_folder_id(self) -> int:
        """
        Returns the ID of the folder in the MailChimp file manager
        which we will upload images into. If the folder doesn't exist,
        it will first create it.
        """
        existing_folders = self.api.folders.all(get_all=True)
        existing_folders = existing_folders['folders']
        for existing_folder in existing_folders:
            if existing_folder['name'] == self.project.remote_image_folder_name:
                id_ = existing_folder['id']
                break
        else:
            logger.info(
                f'Folder "{self.project.remote_image_folder_name}" does not exist '
                f'in MailChimp account "{self.name}", creating it ')
            new_folder = self.api.folders.create(
                data={'name': self.project.remote_image_folder_name})
            id_ = new_folder['id']
        return id_

    @property
    def remote_images(self):
        if self._remote_images is None:
            self._remote_images = self._get_remote_images()
        return self._remote_images

    @log_errors
    def _get_remote_images(self) -> Dict[str, 'RemoteImage']:
        remote_images = {}
        response = self.api.files.all(
            get_all=True,
            type='image',
            fields='files.id,'
                   'files.folder_id,'
                   'files.name,'
                   'files.full_size_url')
        all_images = response['files']
        for existing_image in all_images:
            if existing_image['folder_id'] == self.remote_image_folder_id:
                image = RemoteImage(
                    account_name=self.name,
                    name=existing_image['name'],
                    id=existing_image['id'],
                    folder_id=existing_image['folder_id'],
                    url=existing_image['full_size_url'])
                remote_images[image.key] = image
        return remote_images

    @log_errors
    def upload_image(self, local_image: LocalImage) -> RemoteImage:
        response = self.api.files.create({
            'name': local_image.name,
            'file_data': local_image.as_base_64(),
            'folder_id': self.remote_image_folder_id})
        remote_image = RemoteImage(
            account_name=self.name,
            name=local_image.name,
            id=response['id'],
            folder_id=response['folder_id'],
            url=response['full_size_url'])
        return remote_image

    def get_existing_image(self, local_image: LocalImage) -> Optional[RemoteImage]:
        return self.remote_images.get(f'{local_image.name}_{self.name}', None)

    @log_errors
    def delete_image(self, image_id: int):
        self.api.files.delete(file_id=image_id)

    def __repr__(self):
        return f'Account(name="{self.name}")'
