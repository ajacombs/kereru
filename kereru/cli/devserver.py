import functools
import logging
import signal
import sys
from pathlib import Path
from tempfile import TemporaryDirectory

import click
import livereload

from kereru.image import resolve_image_dev
from kereru.logging import get_logger, default_channel
from kereru.project import Project, ConfigError
from .util import template_callback, withrepr


def tmdir_cleanup_handler(signum, frame, tmpdir: TemporaryDirectory):
    logger = get_logger('livereload')
    try:
        logger.info('Deleting temporary files')
        for file in Path(tmpdir.name).iterdir():
            file.unlink()
        logger.info('Deleting temporary directory')
        tmpdir.cleanup()
    except Exception:
        pass
    logger.info('Exiting server')
    sys.exit()


class Server(livereload.Server):
    """
    Subclass of livereload.Server which overrides the _setup_logging method
    to simplify the formatting of the logging messages.
    """
    def __init__(self):
        super().__init__()
        self.logger = None
        self._setup_logging()

    def _setup_logging(self):
        if self.logger is None:
            self.logger = get_logger('livereload')
            # need a tornado logging handler to prevent IOLoop._setup_logging
            logging.getLogger('tornado').addHandler(default_channel)


@click.command(short_help='Serve a template through a livereload webserver.')
@click.pass_context
@click.argument('template_name', callback=template_callback)
@click.option('-p', '--port', type=int, default=8000)
def devserver(ctx, template_name, port):
    """
    Builds a template, serves it through a local webserver, rebuilds
    the template on any changes, and refreshes the web browser.
    """
    ctx.obj: Project
    template = ctx.obj.templates[template_name]

    tmpdir = TemporaryDirectory()
    tmpdir_path = Path(tmpdir.name)

    sigint_handler = functools.partial(tmdir_cleanup_handler, tmpdir=tmpdir)
    signal.signal(signal.SIGINT, sigint_handler)

    image_resolver_func = functools.partial(
        resolve_image_dev, src_dir=ctx.obj.images_dir, dst_dir=tmpdir_path)

    server = Server()
    server._setup_logging()

    @withrepr(lambda f: 'devbuild')
    def _devbuild():
        nonlocal template
        nonlocal ctx
        try:
            ctx.obj.refresh_from_disk()
            template = ctx.obj.templates[template_name]
        except ConfigError:
            pass
        try:
            html = template.compile(
                build_details=f'{template.version}_DEV',
                image_resolver_func=image_resolver_func,
                dev_mode=True)
            tmpfile = tmpdir_path / 'index.html'
            with tmpfile.open('w', encoding='utf8') as f:
                f.write(html)
        except Exception as e:
            server.logger.error(e)

    _devbuild()
    for path in (str(p) for p in (
        ctx.obj.templates_dir,
        ctx.obj.components_dir,
        ctx.obj.styles_dir,
        ctx.obj.images_dir,
        ctx.obj.config_file,
        ctx.obj.api_keys_file
    )):
        server.watch(path, _devbuild)
    server.serve(root=tmpdir_path)
