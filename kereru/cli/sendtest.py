import functools
import smtplib
from email.message import EmailMessage
from socket import gaierror

import click
import lxml.html

from kereru.account import resolve_image_mailchimp
from kereru.logging import get_logger
from kereru.project import Project
from kereru.cli.util import template_callback, account_callback

logger = get_logger('kereru')


@click.command(short_help='Send a test email of a template.')
@click.pass_context
@click.option(
    '-t', '--template', 'template_name',
    required=True, callback=template_callback, prompt=True)
@click.option(
    '-a', '--account', 'account_name',
    required=True, callback=account_callback, prompt=True)
@click.option('--to', 'to_address', required=True, prompt=True)
@click.option('--from', 'from_address', required=True, prompt=True)
@click.option('--subject', required=True, prompt=True)
@click.option('--smtp-host', required=True, prompt=True)
@click.option('--smtp-user', required=True, prompt=True)
@click.option('--smtp-password', required=True, prompt=True, hide_input=True)
@click.option('--disable-media-queries', is_flag=True)
def sendtest(
        ctx: click.Context,
        template_name: str,
        account_name: str,
        to_address: str,
        from_address: str,
        subject: str,
        smtp_host: str,
        smtp_user: str,
        smtp_password: str,
        disable_media_queries: bool
):
    ctx.obj: Project
    template = ctx.obj.templates[template_name]
    account = ctx.obj.accounts[account_name]
    # Compile the html
    logger.info('Compiling the template')
    image_resolver_func = functools.partial(resolve_image_mailchimp, account=account)
    html = template.compile(
        build_details=template.build_message,
        image_resolver_func=image_resolver_func,
        disable_media_queries=disable_media_queries,
        inline_css=True
    )
    # Build the plaintext message
    parsed_html = lxml.html.fromstring(html)
    raw_textcontent = parsed_html.body.text_content()
    plaintext = '\n'.join(
        line.strip()
        for line
        in raw_textcontent.splitlines()
        if line.strip() not in ('', '\n')
    ).strip()
    # Build the email message
    msg = EmailMessage()
    msg['From'] = from_address
    msg['To'] = to_address
    msg['Subject'] = subject
    msg.set_content(plaintext)
    msg.add_alternative(html, subtype='html')
    # Send the message
    try:
        logger.info(f'Connecting to SMTP server "{smtp_host}"')
        with smtplib.SMTP_SSL(smtp_host) as smtp_server:
            smtp_server.login(smtp_user, smtp_password)
            logger.info('Successfully connected to server, sending email')
            smtp_server.send_message(msg)
            logger.info('Successfully sent email')
    except TimeoutError:
        logger.fatal(f'Connection to SMTP server "{smtp_host}" timed out')
    except gaierror:
        logger.fatal(f'DNS lookup of SMTP server "{smtp_host}" failed')
    except smtplib.SMTPAuthenticationError:
        logger.fatal(f'Authentication with SMTP server "{smtp_host}" failed')
