import sys
from pathlib import Path

import click

from kereru.cli.build import build
from kereru.cli.deploy import deploy
from kereru.cli.devserver import devserver
from kereru.cli.sendtest import sendtest
from kereru.logging import get_logger
from kereru.project import Project, ConfigError

logger = get_logger('kereru')


@click.group()
@click.pass_context
@click.option(
    '-c', '--config',
    type=click.Path(exists=True, dir_okay=False),
    default='config.yml')
@click.option(
    '-a', '--accounts',
    type=click.Path(exists=True, dir_okay=False),
    default='apikeys.yml')
def cli(ctx, config, accounts):
    config = Path(config)
    accounts = Path(accounts)
    ctx.ensure_object(dict)
    try:
        project = Project(config, accounts)
        ctx.obj = project
    except ConfigError:
        sys.exit()


cli.add_command(build)
cli.add_command(devserver)
cli.add_command(deploy)
cli.add_command(sendtest)


if __name__ == '__main__':
    from click.testing import CliRunner
    runner = CliRunner()
    runner.invoke(cli, sys.argv[1:], catch_exceptions=False)
