import functools

import click

from kereru.project import Project
from kereru.logging import get_logger


logger = get_logger('kereru')


def template_callback(ctx: click.Context, param: str, value: str):
    ctx.obj: Project
    if value in ctx.obj.templates or value is None:
        return value
    else:
        all_templates = '\n'.join(f'- {name}' for name in ctx.obj.templates.keys())
        raise click.BadParameter(
            f'There is no template named "{value}" defined in the config file. '
            f'Must be one of:\n{all_templates}')


def account_callback(ctx: click.Context, param: str, value: str):
    ctx.obj: Project
    if value in ctx.obj.accounts or value is None:
        return value
    else:
        all_accounts = '\n'.join(f'- {name}' for name in ctx.obj.accounts.keys())
        raise click.BadParameter(
            f'There is no account named "{value}" defined in the API keys file. '
            f'Must be one of:\n{all_accounts}')


class ReprWrapper:
    """
    Shim class to allow functions to have a custom __repr__. Python doesn't
    allow overriding methods on built in classes, including the function class,
    so we wrp the function in this to allow specifying the __repr__ function.
    """
    def __init__(self, repr_func, func):
        self._repr = repr_func
        self._func = func
        functools.update_wrapper(self, func)

    def __call__(self, *args, **kwargs):
        return self._func(*args, **kwargs)

    def __repr__(self):
        return self._repr(self._func)


def withrepr(repr_func):
    """
    Decorator to add a custom __repr__ to a function.
    """
    def _wrap(func):
        return ReprWrapper(repr_func, func)
    return _wrap
