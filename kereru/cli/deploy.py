import click

from kereru.logging import get_logger
from kereru.project import Project
from .util import template_callback, account_callback

logger = get_logger('kereru')


@click.command(short_help='Upload a template to a MailChimp account.')
@click.pass_context
@click.argument('template_name', callback=template_callback)
@click.argument('account_name', callback=account_callback)
@click.option('--ignore-image-cache', is_flag=True)
@click.option('--replace-all-images', is_flag=True)
@click.option('--replace-changed-images', is_flag=True)
def deploy(
    ctx,
    template_name: str,
    account_name: str,
    ignore_image_cache: bool,
    replace_all_images: bool,
    replace_changed_images: bool
):
    ctx.obj: Project
    template = ctx.obj.templates[template_name]
    account = ctx.obj.accounts[account_name]
    account.deploy_template(
        template=template,
        use_cached_images=not ignore_image_cache,
        replace_all_images=replace_all_images,
        replace_changed_images=replace_changed_images)
