import datetime as dt
import functools
from pathlib import Path

import click

from kereru.account import resolve_image_mailchimp
from kereru.image import resolve_image_filename
from kereru.logging import get_logger
from kereru.project import Project
from .util import template_callback, account_callback

logger = get_logger('kereru')


@click.command(short_help='Compile a template to a static HTML file.')
@click.pass_context
@click.argument(
    'template_name',
    callback=template_callback
)
@click.option(
    '-o', '--outfile',
    type=click.Path(),
    help='The file to save the built template into. '
         'Default is "name_version_timestamp.html"'
)
@click.option(
    '-i', '--imageaccount', 'account_name',
    type=str,
    callback=account_callback
)
@click.option(
    '--disable-media-queries',
    is_flag=True
)
@click.option(
    '--inline-css',
    is_flag=True
)
def build(ctx, template_name, outfile, account_name, disable_media_queries, inline_css):
    ctx.obj: Project
    template = ctx.obj.templates[template_name]
    if account_name:
        account = ctx.obj.accounts[account_name]
        image_resolver_func = functools.partial(resolve_image_mailchimp, account=account)
    else:
        image_resolver_func = resolve_image_filename
    html = template.compile(
        build_details=template.build_message,
        image_resolver_func=image_resolver_func,
        disable_media_queries=disable_media_queries,
        inline_css=inline_css
    )
    if not outfile:
        timestamp = dt.datetime.strftime(dt.datetime.now(), '%Y-%m-%d_%H-%M-%S')
        outfile = Path(f'{template.shortname}_v{template.version}_{timestamp}.html')
    else:
        outfile = Path(outfile)
    outfile.write_text(html, encoding='utf8')
