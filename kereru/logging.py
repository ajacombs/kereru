import functools
import logging
from typing import Callable

from mailchimp3.mailchimpclient import MailChimpError
from requests.exceptions import RequestException


default_channel = logging.StreamHandler()
default_formatter = logging.Formatter(
    fmt='[{asctime}] {message}',
    datefmt='%I:%M:%S %p',
    style='{')
default_channel.setFormatter(default_formatter)


def get_logger(name, level=logging.INFO):
    """
    Returns a Logger object with standardise formatting.
    """
    logger_ = logging.getLogger(name)
    logger_.setLevel(level)
    logger_.addHandler(default_channel)
    return logger_


logger = get_logger('emailbuilder')


def log_errors(_func=None, *, raise_exceptions: bool = False) -> Callable:
    """
    Decorator which logs any exceptions encountered which mailing API calls.
    By default it logs these errors and continues. Setting raise_exeptions
    to True will optionally cause these exceptions to be raised as usual after
    they have been logged.
    """
    def decorator(func: Callable) -> Callable:
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except RequestException as e:
                logger.error(f'Request error while performing '
                             f'MailChimp API call: {e}')
                if raise_exceptions:
                    raise e
            except MailChimpError as e:
                logger.error(f'MailChimp API error: {e}')
                if raise_exceptions:
                    raise e
            except Exception as e:
                logger.error(str(e))
                if raise_exceptions:
                    raise e
        return wrapper
    if _func is None:
        return decorator
    else:
        return decorator(_func)
