import requests
from .logging import log_errors

API_URL = 'https://templates.mailchimp.com/services/inline-css/'


@log_errors
def inline_css(html: str) -> str:
    r = requests.post(API_URL, data={'html': html})
    return r.text
